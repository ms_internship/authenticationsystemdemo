package com.example.ilasgamal.authenticationsystemdemo.AuthenticationModule;

/**
 * Created by ilias on 8/26/2017.
 */

public abstract class EmailAuthenticator implements Authenticator {
    abstract void signIn(String email, String password, OnSignInCallback onSignInCallback);
    abstract void signUp(String email, String password, OnSignUpCallback onSignUpCallback);
    abstract void signOut(OnSignOutCallback onSignOutCallback);
}
