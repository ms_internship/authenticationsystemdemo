package com.example.ilasgamal.authenticationsystemdemo.AuthenticationModule;

/**
 * Created by ilias on 8/25/2017.
 */

import android.app.Activity;
import android.content.Intent;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import java.util.Arrays;


public class FacebookAuthenticator extends SDKAuthenticator {
    private CallbackManager callbackManager;
    private LoginManager loginManager;

    public FacebookAuthenticator() {
        callbackManager = CallbackManager.Factory.create();
        loginManager = LoginManager.getInstance();
    }

    @Override
    public void signIn(Activity activity, final OnSignInCallback onSignInCallback) {
        loginManager.logInWithReadPermissions(activity, Arrays.asList("email", "public_profile"));
        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                if (loginResult.getAccessToken() != null) {
                    onSignInCallback.onSignInSuccess();
                }
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                onSignInCallback.onSignInFailed(error);
            }
        });
    }


    @Override
    public void signOut(OnSignOutCallback onSignOutCallback) {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null) {
            loginManager.logOut();
            loginManager.unregisterCallback(callbackManager);
            onSignOutCallback.onSignOut();
        }
//        loginManager.retrieveLoginStatus(activity, new LoginStatusCallback() {
//            @Override
//            public void onCompleted(AccessToken accessToken) {
//
//            }
//
//            @Override
//            public void onFailure() {
//
//            }
//
//            @Override
//            public void onError(Exception exception) {
//
//            }
//        });
    }

    @Override
    public void checkAuthenticationState(AuthStateChangedCallback authStateChangedCallback) {
        if (AccessToken.getCurrentAccessToken() != null) {
            authStateChangedCallback.onAuthStateChanged(true);
        } else {
            authStateChangedCallback.onAuthStateChanged(false);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
