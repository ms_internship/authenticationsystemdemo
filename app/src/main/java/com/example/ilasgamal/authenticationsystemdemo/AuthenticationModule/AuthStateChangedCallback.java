package com.example.ilasgamal.authenticationsystemdemo.AuthenticationModule;

/**
 * Created by ilias on 8/25/2017.
 */

public interface AuthStateChangedCallback {
    void onAuthStateChanged(boolean isAuthenticated);
}
