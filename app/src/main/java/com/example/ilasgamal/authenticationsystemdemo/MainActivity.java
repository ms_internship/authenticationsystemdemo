package com.example.ilasgamal.authenticationsystemdemo;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.ilasgamal.authenticationsystemdemo.AuthenticationModule.AuthenticationSystem;
import com.example.ilasgamal.authenticationsystemdemo.AuthenticationModule.Authenticator;
import com.example.ilasgamal.authenticationsystemdemo.AuthenticationModule.FacebookAuthenticator;
import com.example.ilasgamal.authenticationsystemdemo.AuthenticationModule.OnSignInCallback;

public class MainActivity extends AppCompatActivity {

    FacebookAuthenticator facebookAuthenticator;
    Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loginButton = (Button) findViewById(R.id.loginButton);
        facebookAuthenticator =
                (FacebookAuthenticator) AuthenticationSystem.getAuthenticator("facebook");
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                facebookAuthenticator.signIn(MainActivity.this, new OnSignInCallback() {
                    @Override
                    public void onSignInSuccess() {
                        Intent intent = new Intent(MainActivity.this, TestLogin.class);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onSignInFailed(@NonNull Exception e) {
                        Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        facebookAuthenticator.onActivityResult(requestCode, resultCode, data);
    }
}
