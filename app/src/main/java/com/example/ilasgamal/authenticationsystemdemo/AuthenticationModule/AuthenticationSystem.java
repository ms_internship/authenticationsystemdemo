package com.example.ilasgamal.authenticationsystemdemo.AuthenticationModule;

/**
 * Created by ilias on 8/25/2017.
 */

public class AuthenticationSystem {
    public static Authenticator getAuthenticator(String authenticatorType){
        if (authenticatorType.equalsIgnoreCase("facebook")){
            return new FacebookAuthenticator();
        }
        return null;
    }
}
