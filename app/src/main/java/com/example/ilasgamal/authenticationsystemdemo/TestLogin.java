package com.example.ilasgamal.authenticationsystemdemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.ilasgamal.authenticationsystemdemo.AuthenticationModule.AuthenticationSystem;
import com.example.ilasgamal.authenticationsystemdemo.AuthenticationModule.FacebookAuthenticator;
import com.example.ilasgamal.authenticationsystemdemo.AuthenticationModule.OnSignOutCallback;

public class TestLogin extends AppCompatActivity {

    FacebookAuthenticator facebookAuthenticator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_login);
        facebookAuthenticator = (FacebookAuthenticator) AuthenticationSystem.getAuthenticator("facebook");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.test_login_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.sign_out:
                facebookAuthenticator.signOut(new OnSignOutCallback() {
                    @Override
                    public void onSignOut() {
                        Intent intent = new Intent(TestLogin.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
