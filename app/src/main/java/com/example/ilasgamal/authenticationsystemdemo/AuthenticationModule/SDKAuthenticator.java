package com.example.ilasgamal.authenticationsystemdemo.AuthenticationModule;

import android.app.Activity;

/**
 * Created by ilias on 8/26/2017.
 */

public abstract class SDKAuthenticator implements Authenticator {
    abstract void signIn(Activity activity, OnSignInCallback onSignInCallback);
    abstract void signOut(OnSignOutCallback onSignOutCallback);
}
