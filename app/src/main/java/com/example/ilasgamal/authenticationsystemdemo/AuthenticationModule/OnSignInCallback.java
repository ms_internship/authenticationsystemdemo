package com.example.ilasgamal.authenticationsystemdemo.AuthenticationModule;

import android.support.annotation.NonNull;

/**
 * Created by ilias on 8/25/2017.
 */

public interface OnSignInCallback {
    void onSignInSuccess();
    void onSignInFailed(@NonNull Exception e);
}
